import { CM } from '@kckckc/isleward-util';
import React from 'react';
import { useSetting } from '../../hooks/useSetting';

const types = ['no', 'both', 'yes'];

export const NicknamesButton = () => {
	const [chatNames, setChatNames] = useSetting('islebuilds.chatNames');

	return (
		<CM.Button
			onClick={() => {
				setChatNames((x) => {
					let n = x + 1;
					if (n < 0 || n >= types.length) {
						n = 0;
					}
					return n;
				});
			}}
		>
			show islebuilds names: {types[chatNames]}
		</CM.Button>
	);
};
