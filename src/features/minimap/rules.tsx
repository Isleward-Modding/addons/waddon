interface MinimapRule {
	ruleName: string;
	ruleId: string;
	enabled?: boolean;

	name?: (string | RegExp)[];
	isPlayer?: boolean;
	isSelf?: boolean;
	isRare?: boolean;

	color: string;
	radius: number;
}

const RULESET_DEFAULT: MinimapRule[] = [
	{
		ruleName: 'All Players',
		ruleId: 'players-all',
		enabled: false,

		isPlayer: true,
		color: '#FFEB38',
		radius: 2,
	},
	{
		ruleName: 'Other Players',
		ruleId: 'players-other',
		enabled: true,

		isPlayer: true,
		isSelf: false,
		color: '#FFEB38',
		radius: 2,
	},
	{
		ruleName: 'Own Player',
		ruleId: 'players-own',
		enabled: true,

		isPlayer: true,
		isSelf: true,
		color: '#FAAC45',
		radius: 2,
	},
	{
		ruleName: 'Bosses',
		ruleId: 'bosses',

		name: [
			"m'ogresh",
			'radulos',
			'sundfehr',
			'stinktooth',
			'bera the blade',
		],
		isPlayer: false,
		color: '#B4347A',
		radius: 2,
	},
	{
		ruleName: 'Rare Mobs',
		ruleId: 'raremobs',

		isRare: true,
		color: '#FC66F7',
		radius: 2,
	},
	{
		ruleName: 'Carp',
		ruleId: 'carp',

		name: ['/carp$/'],
		isPlayer: false,
		color: '#3FA7DD',
		radius: 3,
	},
	{
		ruleName: 'Pumpkins',
		ruleId: 'pumpkins',

		name: ['/pumpkin/'],
		isPlayer: false,
		color: '#FF6942',
		radius: 2,
	},
	{
		ruleName: 'Gifts',
		ruleId: 'gifts',

		name: ['/gift$/'],
		isPlayer: false,
		color: '#FF4252',
		radius: 2,
	},
	{
		ruleName: 'Herbs',
		ruleId: 'herbs',

		name: ['skyblossom', 'emberleaf', 'stinkcap'],
		isPlayer: false,
		color: '#80F643',
		radius: 2,
	},
	{
		ruleName: 'Flabbers',
		ruleId: 'flabbers',

		name: ['flabbers'],
		isPlayer: false,
		color: '#B15A30',
		radius: 2,
	},
	{
		ruleName: 'All Objects',
		ruleId: 'allobjects',
		enabled: false,

		color: '#FCFCFC',
		radius: 2,
	},
];

const fixRules = (rules: MinimapRule[]): MinimapRule[] => {
	for (const rule of rules) {
		if (rule.name) {
			if (!Array.isArray(rule.name)) {
				rule.name = [rule.name];
			}

			rule.name.forEach((nameRule, idx) => {
				if (
					typeof nameRule === 'string' &&
					nameRule.startsWith('/') &&
					nameRule.endsWith('/')
				) {
					nameRule = nameRule.slice(1, nameRule.length - 1);
					if (rule.name) {
						rule.name[idx] = new RegExp(nameRule);
					}
				}
			});
		}
	}

	return rules;
};

export const RULESET = fixRules(RULESET_DEFAULT);

// Defaults
const defaultRulesEnabled: Record<string, boolean> = {};
for (const rule of RULESET) {
	defaultRulesEnabled[rule.ruleId] = rule.enabled ?? true;
}
export { defaultRulesEnabled };
