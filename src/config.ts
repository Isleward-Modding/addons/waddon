export const ISLEBUILDS_WEB_URL = 'https://islebuilds.com';
export const ISLEBUILDS_API_URL = 'https://api.islebuilds.com';
// export const ISLEBUILDS_WEB_URL = 'http://localhost:3000';
// export const ISLEBUILDS_API_URL = 'http://localhost:4000';

export const TICK_DURATION = 350;
