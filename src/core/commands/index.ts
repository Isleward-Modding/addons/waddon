import type { IwdEvents } from '../../isleward.types';
import { requireAsync } from '../../util/requireAsync';
import { sendMessage } from '../../util/sendMessage';

interface ChatEventData {
	success: boolean;
	message: string;
	cancel: boolean;
}

type TriggerType = string | ((input: ChatEventData) => boolean);
export interface Command {
	trigger: TriggerType;
	callback: (data: ChatEventData) => void;
}

const commands: Command[] = [];

const register = (
	trigger: TriggerType,
	callback: (data: ChatEventData) => void,
) => {
	commands.push({ trigger, callback });
};

const onBeforeChat = (msgConfig: ChatEventData) => {
	const matchedCommand = commands.find((cmd) => {
		if (typeof cmd.trigger === 'function') {
			return cmd.trigger(msgConfig);
		} else {
			return msgConfig?.message?.startsWith(cmd.trigger);
		}
	});

	if (matchedCommand) {
		matchedCommand.callback(msgConfig);
	}
};

const init = async () => {
	const events: IwdEvents = await requireAsync('js/system/events');

	events.on('onBeforeChat', onBeforeChat);

	register('/help', () => {
		sendMessage(
			'Waddon adds additional commands: <br/>' +
				commands
					.filter((c) => typeof c.trigger === 'string')
					.map((c) => c.trigger)
					.join('<br />'),
			'color-yellowB',
			'info',
		);
	});
};

const commandsModule = { layer: 'core', init, register };

export default commandsModule;
