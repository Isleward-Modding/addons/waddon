import { requireAsync } from '../../util/requireAsync';

// TODO: onStateChange?

export type GameState = 'login' | 'select' | 'game';

let state: GameState = 'login';

type Listener = (state: GameState) => void;
let listeners: Listener[] = [];

const subscribe = (cb: Listener) => {
	listeners.push(cb);
	cb(state);
};
const unsubscribe = (cb: Listener) => {
	listeners = listeners.filter((r) => r !== cb);
};

const notify = () => {
	listeners.forEach((l) => l(state));
};

const onAfterRenderUi = ({ ui }: { ui: any }) => {
	if (ui.type === 'characters') {
		state = 'select';
		notify();
	} else if (ui.type === 'effects') {
		state = 'game';
		notify();
	}
};

const get = () => {
	return state;
};

const init = async () => {
	const events = await requireAsync('js/system/events');

	// Track playing state
	events.on('onAfterRenderUi', onAfterRenderUi);
};

const stateModule = {
	layer: 'core',

	init,
	get,
	subscribe,
	unsubscribe,
};

export default stateModule;
