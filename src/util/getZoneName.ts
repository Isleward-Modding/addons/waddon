const ZONE_NAMES: Record<string, string> = {
	fjolarok: 'Fjolarok',
	fjolgard: 'Fjolgard',
	sewer: 'Sewer',
	cave: 'Crystal Caves',
	estuary: 'Estuary',
	templeOfGaekatla: 'Temple of Gaekatla',
	pyramidOfTheSun: 'Pyramid of the Sun',
	fjolgardHousing: 'Fjolgard Housing',
};

export const getZoneName = (name: string) => {
	return ZONE_NAMES[name] ?? name;
};
