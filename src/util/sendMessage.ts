import { getWindow } from './getWindow';

export const sendMessage = (message: string, color: string, type = 'info') => {
	getWindow().addons?.events?.emit('onGetMessages', {
		messages: [
			{
				class: color,
				message: message,
				type: type,
			},
		],
	});
};
