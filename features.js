export default [
	'islebuilds',
	'debug',
	'visual',
	'location',
	'qol',
	'chatreplay',
	'farm',
	'timers',
	'xp',
	'minimap',
	'tooltips',
	'charselect',
];
